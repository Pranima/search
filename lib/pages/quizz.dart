import 'package:flutter/material.dart';

class Quiz extends StatefulWidget {
  @override
  _QuizState createState() => _QuizState();
}

class _QuizState extends State<Quiz> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
          child: Scaffold(
        body: Container(
          margin: EdgeInsets.all(20.0),
          //color: Colors.green,
          child: Column(
            children: <Widget>[
              MaterialButton(
                onPressed: (){},
                color: Colors.green[400],
                child: Text('material button'),
                ),

              SizedBox(height: 20.0),

              RaisedButton(
                onPressed: null,
                color: Colors.blue[400],
                child: Text(
                  'raised button',
                  style: TextStyle(
                    color: Colors.white,
                  ),
                  ),
                ),  
            ],
          ),
        ),
      ),
    );
  }
}