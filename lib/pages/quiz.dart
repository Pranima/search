
import 'package:flutter/material.dart';
import 'package:project_search/pages/summary.dart';

class AnimalQuiz{
  List<String> questions = [
    'This animal is a carnivorous reptile.',
    '_____________ like to chase mice and birds.',
    'Give a ______ a bone and he will find his way home.',
    'A nocturnal animal with some big eyes.'
  ];

  var choices = [
    ['Cat', 'Sheep', 'Alligator', 'Cow'],
    ['Cat', 'Mouse', 'Slug', 'Horse'],
    ['Mouse', 'Dog', 'Elephant', 'Donkey'],
    ['Spider', 'Snake', 'Hawk', 'Owl']
  ];

  var correctAnswers = [
    'Alligator', 'Cat', 'Dog', 'Owl'
  ];
}

var questionNumber = 0;
var finalScore = 0;
var quiz = new AnimalQuiz();



/*class Quiz extends StatefulWidget {
  @override
  _QuizState createState() => _QuizState();
}

class _QuizState extends State<Quiz> {

 // List<String> options = ['a. nose', 'b. ear', 'c. eye', 'd. tongue'];

  Widget optionTemplate(option){
    return Card(
      child: 
       Container(
         padding: EdgeInsets.all(5),
         margin: EdgeInsets.all(10),
         child: Text (
           option,
           textAlign: TextAlign.start,
           style: TextStyle(
             fontSize: 15.0,
           ),
         ),
       ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
          child: Scaffold(
        body: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Center(
              child: Container(
                margin: EdgeInsets.all(15.0),
                child: Text(
                  'Which of the following helps us to see?',
                  style: TextStyle(
                    fontSize: 15.0,
                  ),
                ),
              ),
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: options.map((option) => optionTemplate(option)).toList(),
            ),
            ],
        ),
      ),
    );
  }
}*/

class Quiz extends StatefulWidget {
  @override
  _QuizState createState() => _QuizState();
}

class _QuizState extends State<Quiz> {

  void updateQuestion(){
    setState(() {
      if (questionNumber==quiz.questions.length-1){
        Navigator.push(context, MaterialPageRoute(builder: (context) => Summary()));
      }else{
        questionNumber++;
      }
      
    });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(

          child: Scaffold(
            body: Container(
              color: Colors.grey[850],

              child: Column(
                children: <Widget>[

                  Padding(padding: EdgeInsets.all(12.0),),

                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[

                      Text(
                        'Question ${questionNumber+1} of ${quiz.questions.length}',
                        style: TextStyle(
                          fontSize: 22.0,
                          color: Colors.amber[300],
                        ),
                      ), 

                      
                    ],
                  ),

                  SizedBox(height: 50,),

                      Text(
                        
                        quiz.questions[questionNumber],
                        style: TextStyle(
                          fontSize: 20.0,
                          color: Colors.grey[300],
                        ),
                      ),

                      SizedBox(height: 40,),

                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[

                          //button1
                          MaterialButton(
                            minWidth: 250.0,
                            color: Colors.pink[100],
                            shape: StadiumBorder(),
                            onPressed: (){
                              if (quiz.choices[questionNumber][0] == quiz.correctAnswers[questionNumber]){
                                print('correct');
                                finalScore++;
                              }
                              else{
                                print('incorrect');
                              }
                              updateQuestion();
                            },
                            child: Text(
                              quiz.choices[questionNumber][0],
                              style: TextStyle(
                                fontSize: 20.0,
                                color: Colors.white,
                              ),
                            ),
                          ), 
                                                  
                        ],
                      ),
                  
                      //SizedBox(height: 20,),

                      //button2
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              MaterialButton(
                                minWidth: 250.0,
                                color: Colors.pink[100],
                                shape: StadiumBorder(),
                                onPressed: (){
                                  if (quiz.choices[questionNumber][1] == quiz.correctAnswers[questionNumber]){
                                    debugPrint('correct');
                                    finalScore++;
                                  }
                                  else{
                                    print('incorrect');
                                  }
                                  updateQuestion();
                                },
                                child: Text(
                                  quiz.choices[questionNumber][1],
                                  style: TextStyle(
                                    fontSize: 20.0,
                                    color: Colors.white,                            
                                  ),
                                ),
                              ),
                            ],
                          ), 

                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[

                          //button3
                          MaterialButton(
                            minWidth: 250.0,
                            color: Colors.pink[100],
                            shape: StadiumBorder(),
                            onPressed: (){

                              if (quiz.choices[questionNumber][2] == quiz.correctAnswers[questionNumber]){
                                debugPrint('correct');
                                finalScore++;
                              }
                              else{
                                print('incorrect');
                              }
                              updateQuestion();

                            },
                            child: Text(
                              quiz.choices[questionNumber][2],
                              style: TextStyle(
                                fontSize: 20.0,
                                color: Colors.white,
                              ),
                            ),
                          ), 
                          
                        ],
                      ),
                      //button4
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              MaterialButton(
                                minWidth: 250.0,
                                color: Colors.pink[100],
                                shape: StadiumBorder(),
                                onPressed: (){
                                  if (quiz.choices[questionNumber][3] == quiz.correctAnswers[questionNumber]){
                                    debugPrint('correct');
                                    finalScore++;
                                  }
                                  else{
                                    print('incorrect');
                                  }
                                  updateQuestion();
                                },
                                child: Text(
                                  quiz.choices[questionNumber][3],
                                  style: TextStyle(
                                    fontSize: 20.0,
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                            ],
                          ), 
                ],
              ),
            ),
      ),
    );
  }

  
}
























