
import 'package:flutter/material.dart';
import 'package:project_search/pages/home.dart';
import 'package:project_search/pages/loading.dart';
import 'package:project_search/pages/quiz.dart';


//import 'package:project_search/pages/home.dart';
import 'package:project_search/pages/settings.dart';

void main() => runApp(MaterialApp(
  initialRoute: '/home',
  //home: Home(),
  routes: {
    '/': (context) => Loading(),
    '/home': (context) => Home(),
    '/settings': (context) => Setting(),
    '/quiz': (context) => Quiz(),
  },
));
